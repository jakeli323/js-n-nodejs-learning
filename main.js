var fs = require("fs");


//*********
//学习：同步文件读取
//************
// var data = fs.readFileSync('input.txt');


//*********
//   学习：异步文件读取
//************
// fs.readFile('input.txt', function(err, data){
//     if(err) return console.error(err);
//     console.log(data.toString());
// });


//*********
//*     学习：event
//************
// var events = require('events');

// var eventEmitter = new events.EventEmitter();

// var connectHandler = function connected(){
//     console.log('connection successful');

//     eventEmitter.emit('data_received');
// }

// eventEmitter.on('connection', connectHandler);

// eventEmitter.on('data_received', function(){
//     console.log('data received successful');
// });

// eventEmitter.emit('connection');

//*********
//学习：listener
//************
// var events = require('events');
// var eventEmitter = new events.EventEmitter();

// var listener1 = function listener1(){
//     console.log('listener1 executed');
// }

// var listener2 = function listener2(){
//     console.log('listener2 executed');
// }

// eventEmitter.addListener('connection', listener1);
// eventEmitter.on('connection', listener2);


// var eventListeners = require('events').EventEmitter.listenerCount(eventEmitter, 'connection');

// // console.log("count = " + eventListeners);

// // eventEmitter.emit('connection');
// console.log(eventListeners + " Listeners listening to connection event");


// eventEmitter.emit('connection');

// eventEmitter.removeListener('connection', listener1);
// console.log("listener 1 will not listen now");

// eventEmitter.emit('connection');

// eventListeners = require('events').EventEmitter.listenerCount



 
//*********
//学习：buffer
//************
//创建256字节的缓冲
// var buf = new Buffer.alloc(256);
// var len = buf.write('Simply Easy Learning');

// // console.log('Octets written......  ' + len);

// var json = buf.toJSON(buf);

// console.log(json);


 
//*********
//学习：stream  read
//************
// var fs = require("fs");
// var data = '';

// var readerStream = fs.createReadStream('input.txt');
// readerStream.setEncoding('UTF-8');

// readerStream.on('data', function(chunk){
//     console.log("\n1111111\n");
//     data += chunk;
// });

// readerStream.on('end', function(){
//     console.log(data);
// });

// readerStream.on('error', function(){
//     console.log(err.stack);
// });


//******
//学习：stream write
//********
// var data = require("fs");
// var data = 'Simply Easy Learning';

// var writerStream = fs.createWriteStream('output.txt');

// writerStream.write(data, 'UTF8');

// writerStream.end();

// writerStream.on('finish', function(){
//     console.log("Write completed");
// });




//*********
//学习：object
//************
// var obj = {name:'jakeli', age:11, profession:'programmer', info:{address:"jmpt", road:"jianghua"}, funcName:function(){
//     return this.name + "   " + this.age;
// }};
// console.log(obj.profession);
// console.log(obj.info.address);
// console.log(obj.funcName());

//*********
//学习：array
//************
// var arr = [1, 2, 3, 4];
// console.log(arr[0]);
// console.log("len = " + arr.length);



//*********
//学习：open  read close truncate 文件 
//************
// var fs = require("fs");
// var buf = Buffer.alloc(1024);

// console.log("Going to open file！");
// fs.open('input.txt', 'r+', function(err, fd){
//     if(err){
//         return console.error(err);
//     }

//     console.log("file opened successfully");
//     console.log("Going to read the same file");

//     fs.truncate(fd, 10, function(err){
//         if(err){
//             console.log(err);
//         }

//         fs.read(fd, buf, 0, buf.length, 0, function(err, bytes){
//             if(err){
//                 console.log(err);
//             }

//             if(bytes > 0){
//                 console.log(buf.slice(0, bytes).toString());
//             }

//             fs.close(fd, function(err){
//                 if(err){
//                     console.log(err);
//                 }
//                 console.log("closed");

//             });
//         });
 
//     });
 
// });





//******
//学习： file type
//********
// var fs = require("fs");
// console.log("Going to get file info!");

// fs.stat('input.txt', function(err, stats){
//     if(err){
//         return console.err(err);
//     }

//     console.log(stats);
//     console.log("Got file info successfully");

//     console.log("isfile ?  " + stats.isFile());
//     console.log("isDirectory ?  " + stats.isDirectory());
// });


//*******
//学习：write read 文件
 // var fs = require("fs");

 // console.log("Going to write into existing file");
 // fs.writeFile("input.txt", "Simply easy leanringjjjjjjj!!!", function(err){
 //    if(err){
 //        return console.log(err);
 //    }

 //    console.log("Data written successfully");

 //    fs.readFile('input.txt', function(err, data){
 //        if(err){
 //            return console.error(err);
 //        }

 //        console.log("Asynchronous read: " + data.toString());
 //    });

 // });


 //****
 //学习：delete 删除文件
// var fs = require('fs');

// console.log("Going to delete an existing file");

// fs.unlink("input.txt", function(err){
// if(err){
//     return console.error(err);
// }

// console.log("File deleted successfully");

// });


//******
//学习：nodejs 的 path概念  nodejs的文件路径https://github.com/jawil/blog/issues/18
//*******
// var path = require('path');
// console.log(path.resolve("./"));
// console.log(path.resolve("../nodejstest/tmp"));
// console.log(path.resolve("/"));
// console.log(__dirname);
// console.log(process.cwd());






//******
//文件：read directory 读取文件夹  便利文件夹  
//****
// var fs = require('fs');
// var path = require('path');

// console.log(process.cwd());

// console.log("Going to read directory /tmp");

// fs.readdir("./tmp/", function(err, files){
//     if(err){
//         return console.error(err);
//     }

//     files.forEach(function(file){
//         console.log(file);
//     });
// });



console.log("Program end.....");

 




